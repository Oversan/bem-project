module.exports = {
    block: 'page',
    title: 'contacts',
    head: [
        { elem: 'css', url: 'contacts.min.css' }
    ],
    scripts: [{ elem: 'js', url: 'contacts.min.js' }],
    content: [
       {
           block: 'content',
           content: [
               'block content'
           ]
       }
    ]
};
